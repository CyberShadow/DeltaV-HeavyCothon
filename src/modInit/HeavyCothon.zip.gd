extends Node

func _ready():
	Debug.l("HEAVY-COTHON: started loading HeavyCothon mod")

	var loadedScript

	var scene = preload("res://enceladus/Dealer.tscn")
	var modifiedScene = scene.instance()

	ModLoader.appendNodeInScene(modifiedScene, "res://enceladus/Dealer.tscn", "HEAVY-COTHON", "VP/Viewport", "res://ships/dealer/showroom-Cothon.tscn", false)

	ModLoader.appendTranslationCsv("res://modInit/translation.csv")
	#ModLoader.appendTranslationJson("res://modInit/translation.json")

	loadedScript = load("res://override/ships/Shipyard.gd").new()
	for key in loadedScript.ships:
		Shipyard.ships[key] = loadedScript.ships[key]
	for key in loadedScript.defaultShipConfig:
		Shipyard.defaultShipConfig[key] = loadedScript.defaultShipConfig[key]
	for key in loadedScript.usedShipConfigs:
		Shipyard.usedShipConfigs[key] = loadedScript.usedShipConfigs[key]

	loadedScript = load("res://override/CurrentGame.gd").new()
	CurrentGame.usedShipsPool = CurrentGame.usedShipsPool + loadedScript.usedShipsPool

	Debug.l("HEAVY-COTHON: finished loading HeavyCothon mod")